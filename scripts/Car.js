// VARIABLES
const GROUND_FRICTION = 0.98;
const DRIVE_POWER = 0.5;
const REVERSE_POWER = 0.5;
const TURN_RATE = 0.1;
const MIN_TURN_SPEED = 0.2;

var Car = function(carName, carSprite) {

	this.x = 0;
	this.y = 0;
	this.rotation = 0;
	this.velocity = 0;
	this.name = carName;
	this.sprite = carSprite;

	this.keyHeld_Gas       = false;
	this.keyHeld_Reverse   = false;
	this.keyHeld_TurnLeft  = false;
	this.keyHeld_TurnRight = false;

	this.controlKeyUp;
	this.controlKeyDown;
	this.controlKeyLeft;
	this.controlKeyRight;


	this.setupInput = function(upKey, downKey, leftKey, rightKey) {
		this.controlKeyUp = upKey;
		this.controlKeyDown = downKey;
		this.controlKeyLeft = leftKey;
		this.controlKeyRight = rightKey;
	}

	this.move = function() {

		this.velocity *= GROUND_FRICTION;
		if(this.velocity > -0.1 && this.velocity < 0.1) { this.velocity = 0 };

		if(this.keyHeld_Gas) { this.velocity += DRIVE_POWER; }

		if(this.keyHeld_Reverse) { this.velocity -= REVERSE_POWER; }

		if(Math.abs(this.velocity) > MIN_TURN_SPEED){
			if(this.keyHeld_TurnRight) { this.rotation += TURN_RATE; }
			if(this.keyHeld_TurnLeft) { this.rotation -= TURN_RATE; }
		}

		this.x += Math.cos(this.rotation) * this.velocity;
		this.y += Math.sin(this.rotation) * this.velocity;

		carTrackHandling(this);
	}

	this.reset = function() {
		for(var row = 0; row < TRACK_ROWS; row++) {
			for(var col = 0; col < TRACK_COLS; col++) {
				if(trackGrid[getTrackIndexColRow(col,row)] == TRACK_PLAYER_START){
					trackGrid[getTrackIndexColRow(col,row)] = TRACK_ROAD;
					this.x = col * TRACK_W + (TRACK_W/2);
					this.y = row * TRACK_H + (TRACK_H/2);
					return;
				}
			}
		}
	}

	this.draw = function() {
		drawCenteredBitmap(this.sprite, this.x, this.y, this.rotation);
	}

}
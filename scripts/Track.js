const TRACK_W = 40;
const TRACK_H = 40;
const TRACK_GAP = 0;
const TRACK_ROWS = 15;
const TRACK_COLS = 20;
var level1 =    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				 1, 1, 1, 1, 0, 0, 0, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
				 1, 1, 1, 0, 0, 0, 0, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,
				 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1,
				 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1,
				 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1,
				 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1,
				 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1,
				 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1,
				 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1,
				 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1,
				 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

var trackGrid = [];

const TRACK_ROAD = 0;
const TRACK_GRASS = 1;
const TRACK_PLAYER_START = 2;
const TRACK_GOAL = 3;

	
var returnTileTypeAtColRow = function(col, row) {
	if(col >= 0 && col < TRACK_COLS &&
	   row >= 0 && row < TRACK_ROWS) {
		var trackIndexAtCoord = getTrackIndexColRow(col, row);
		return trackGrid[trackIndexAtCoord];
	} else {
		return TRACK_GRASS;
	}
}

var carTrackHandling = function(car) {
	var trackCol = Math.floor(car.x/TRACK_W);
	var trackRow = Math.floor(car.y/TRACK_H);
	var cellIndex = getTrackIndexColRow(trackCol, trackRow);

	if(trackCol >= 0 && trackCol < TRACK_COLS &&
	   trackRow >= 0 && trackRow < TRACK_ROWS) {

		var tileType = returnTileTypeAtColRow(trackCol, trackRow);

		if(tileType == TRACK_GOAL) {
			console.log('reached', car.name);
			loadLevel(level1);
		} else if (tileType != TRACK_ROAD) {
	   		car.x -= Math.cos(car.rotation) * car.velocity;
	   		car.y -= Math.sin(car.rotation) * car.velocity;
	   		car.velocity *= -0.08;
	   	}
	}
}

var getTrackIndexColRow = function(col, row) {
	return col + row * TRACK_COLS;
}

var drawTracks = function() {

	var arrayIndex = 0;
	var tileX = 0;
	var tileY = 0;

	for(var row = 0; row < TRACK_ROWS; row++) {
		for(var col = 0; col < TRACK_COLS; col++) {

			var typeOfTile = trackGrid[arrayIndex];
			var sprite = trackSprites[typeOfTile];

			drawBitmap(sprite, tileX, tileY);

			tileX += TRACK_W;
			arrayIndex++;
		}
		tileX = 0;
		tileY += TRACK_H;
	}
}


var blueCarSprite = document.createElement("img");
var greenCarSprite = document.createElement("img");
var redCarSprite = document.createElement("img");
var yellowCarSprite = document.createElement("img");

var trackSprites = [];
var spritesToLoad = 0; // set automatically in loadSprites()

var loadSprite = function(spriteVar, fileName){
	spriteVar.onload = spriteLoaded;
	spriteVar.src = "images/" + fileName;
}

var loadTrackSprite = function(spriteCode, fileName){
	trackSprites[spriteCode] = document.createElement("img");
	trackSprites[spriteCode].onload = spriteLoaded;
	trackSprites[spriteCode].src = "images/" + fileName;
}

var loadSprites = function(){

	var spriteList = [
		{varName:blueCarSprite, fileName:"car-blue.png"},
		{varName:greenCarSprite, fileName:"car-green.png"},
		{varName:redCarSprite, fileName:"car-red.png"},
		{varName:yellowCarSprite, fileName:"car-yellow.png"},

		{trackType:TRACK_ROAD, fileName:"tile-road.png"},
		{trackType:TRACK_GRASS, fileName:"tile-grass.png"},
		{trackType:TRACK_GOAL, fileName:"tile-goal.png"}
	];

	spritesToLoad = spriteList.length;

	for(var i = 0; i < spriteList.length; i++){
		if(spriteList[i].varName != undefined){
			loadSprite(spriteList[i].varName, spriteList[i].fileName);
		} else {
			loadTrackSprite(spriteList[i].trackType, spriteList[i].fileName);
		}
	}
}

var spriteLoaded = function(){
	spritesToLoad --;
	if(spritesToLoad == 0) { startGame(); }
}
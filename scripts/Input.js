const KEY_UP = 38;
const KEY_DOWN = 40;
const KEY_LEFT = 37;
const KEY_RIGHT = 39;

const KEY_W = 87;
const KEY_A = 65;
const KEY_S = 83;
const KEY_D = 68;


var setupInput = function() {
	document.addEventListener("mousemove", mouseMove);
	document.addEventListener("click", mouseClick);
	document.addEventListener("keydown", keyDown);
	document.addEventListener("keyup", keyUp);

	player1.setupInput(KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT);
	player2.setupInput(KEY_W, KEY_S, KEY_A, KEY_D);
}

var mouseMove = function(evt) {
	// console.log(evt.clientX, evt.clientY);
}

var mouseClick = function(evt) {
	// console.log(carX, carY);
}

var setKey = function(keyEvent, car, setTo) {
	if(keyEvent.keyCode === car.controlKeyUp) {
		car.keyHeld_Gas = setTo;
	}

	if(keyEvent.keyCode === car.controlKeyDown) {
		car.keyHeld_Reverse = setTo;
	}

	if(keyEvent.keyCode === car.controlKeyLeft) {
		car.keyHeld_TurnLeft = setTo;
	}

	if(keyEvent.keyCode === car.controlKeyRight) {
		car.keyHeld_TurnRight = setTo;
	}
}

var keyDown = function(evt) {
	setKey(evt, player1, true);
	setKey(evt, player2, true);
}

var keyUp = function(evt) {
	setKey(evt, player1, false);
	setKey(evt, player2, false);
}
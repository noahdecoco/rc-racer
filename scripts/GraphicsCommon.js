// DRAW FUNCTIONS
var drawRectangle = function(_x, _y, _width, _height, _colour) {
	canvasContext.fillStyle = _colour;
	canvasContext.fillRect(_x, _y, _width, _height);
}

var drawCircle = function(_x, _y, _radius, _colour) {
	canvasContext.fillStyle = _colour;
	canvasContext.beginPath();
	canvasContext.arc(_x, _y, _radius, 0, Math.PI*2, true);
	canvasContext.fill();	
}

var drawBitmap = function(_bitmap, _x, _y) {
	canvasContext.drawImage(_bitmap, _x, _y);
};

var drawCenteredBitmap = function(_bitmap, _x, _y, _rotation) {
	canvasContext.save();
	canvasContext.translate(_x, _y);
	canvasContext.rotate(_rotation);
	canvasContext.drawImage(_bitmap, -_bitmap.width/2, -_bitmap.height/2);
	canvasContext.restore();
};

var drawText = function(_text, _x, _y, _colour) {
	canvasContext.fillStyle = _colour;
	canvasContext.fillText(_text, _x, _y);
	canvasContext.fill();
}

var clearScreen = function() {
	drawRectangle(0, 0, canvas.width, canvas.height, "#261213");
}
// VARIABLES
var canvas, canvasContext;
var framesPerSecond = 30;

var player1;
var player2;

window.onload = function() {
	canvas = document.getElementById("gameCanvas");
	canvasContext = canvas.getContext("2d");

	drawRectangle(0, 0, canvas.width, canvas.height, "#000000");
	drawText("Loading...", canvas.width/2, canvas.height/2, "#ffffff");

	loadSprites();
}

var startGame = function(){
	loadLevel(level1);
	
	setInterval(moveAll, 1000/framesPerSecond);
	window.requestAnimationFrame(drawAll);
}

var loadLevel = function(level){
	trackGrid = level.slice();
	player1 =  new Car("Player 1", blueCarSprite);
	player1.reset();
	player2 =  new Car("Player 2", greenCarSprite);
	player2.reset();
	setupInput();
}


var drawAll = function() {
	// clearScreen();
	drawTracks();
	player1.draw();
	player2.draw();
	window.requestAnimationFrame(drawAll);
}

var moveAll = function() {
	player1.move();
	player2.move();
}